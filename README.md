# Open-SkyEye

## 前言
SkyEye是一款强大的国产嵌入式模拟器，本仓库对其早期的开源版本进行整理，并提供构建编译和安装的教程。对嵌入式软件仿真系统感兴趣的小伙伴，可以 `Star`我！

## SkyEye介绍

<img src='doc/image/logo.png'/>
<br>

SkyEye是一个开源软件（OpenSource Software）项目，中文名字是"天目"。SkyEye的目标是在通用的Linux和Windows平台上实现一个纯软件集成开发环境，模拟常见的嵌入式计算机系统(这里假定"仿真"和"模拟"的意思基本相同)；可在SkyEye上运行μCLinux以及μC/OS-II等多种嵌入式操作系统和各种系统软件（如TCP/IP，图形子系统，文件子系统等），并可对它们进行源码级的分析和测试。

关于开源版本的更多介绍：[点击我跳转](https://www.oschina.net/p/skyeye?hmsr=aladdin1e1)

目前开源版本停留在1.3.5版本，后期版本由浙江迪捷软件科技有限公司开发和维护，[点击我查看SkyEye的最新介绍](https://www.digiproto.com/product/24.html)。


## 仓库介绍

### 仓库结构

| 一级目录 |     二级目录     | 三级目录 |        说明        |
| :------: | :--------------: | :------: | :----------------: |
|   doc    |      image       |   ---    |      存放图片      |
|          |      guide      |  v1.3.4  |  配套的开发者手册  |
| release  |   skyeye-1.3.5   |   ---    | 发行版本及运行环境 |



## build教程


> 参考文档：

> 编译构建安装教程：doc\guide\v1.3.4\Installation guide.docx

> API手册：doc\guide\v1.3.4\skyeye_api_v6.pdf

> 开发者文档：doc\guide\v1.3.4\skyeyev3_usermanual-v6.pdf
>              doc\guide\v1.3.4\skyeye_internal_v6.pdf